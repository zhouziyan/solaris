#include <stdio.h> 
#include <string.h> 
#include <assert.h>
#include <security/cryptoki.h>
#include <cryptoutil.h>

#define PRIVATE                 0x01
#define TOKEN                   0x02
#define SENSITIVE               0x04
#define EXTRACTABLE             0x08

static unsigned char aes_ccm_key[] = {
	0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
	0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf
};

static unsigned char aes_ccm_nonce[] = {
	0x00, 0x00, 0x00, 0x03, 0x02, 0x01, 0x00, 0xa0,
	0xa1, 0xa2, 0xa3, 0xa4, 0xa5
};

static unsigned char aes_ccm_adata[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07
};

static unsigned char aes_ccm_plaintext[] = {
	0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
	0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
	0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e
};

static unsigned char aes_ccm_ciphertext[] = {
	0x58, 0x8c, 0x97, 0x9a, 0x61, 0xc6, 0x63, 0xd2,
	0xf0, 0x66, 0xd0, 0xc2, 0xc0, 0xf9, 0x89, 0x80,
	0x6d, 0x5f, 0x6b, 0x61, 0xda, 0xc3, 0x84, 0x17,
	0xe8, 0xd1, 0x2c, 0xfd, 0xf9, 0x26, 0xe0
};

/* parameter for encryption */
CK_CCM_PARAMS aes_ccm_params_enc = {
	.ulMACLen = 8,
	.ulDataLen = sizeof (aes_ccm_plaintext),
	.pNonce = aes_ccm_nonce,
	.ulNonceLen = sizeof (aes_ccm_nonce),
	.pAAD = aes_ccm_adata,
	.ulAADLen = sizeof (aes_ccm_adata)
};

/* parameter for decryption */
CK_CCM_PARAMS aes_ccm_params_dec = {
	.ulMACLen = 8,
	.ulDataLen = sizeof (aes_ccm_ciphertext),
	.pNonce = aes_ccm_nonce,
	.ulNonceLen = sizeof (aes_ccm_nonce),
	.pAAD = aes_ccm_adata,
	.ulAADLen = sizeof (aes_ccm_adata)
};

static void string2hex(unsigned char *pBuf, int num)
{
	int i, j = 0;

	for (i = 0; i<num; i++, j++) {
		if (j % 8 == 0)
			printf("\n");
		printf("0x%02x ", pBuf[i]);
	}
	printf("\n");
	return;
}

static CK_RV create_symmetric_key(CK_SESSION_HANDLE sh, 
		                  CK_KEY_TYPE keytype,
			          uint32_t permission, 
			   	  CK_OBJECT_HANDLE *keyhandle, 
			   	  char *label,
			   	  CK_BYTE *key, 
			   	  CK_ULONG keylen)
{
	CK_OBJECT_CLASS	secretKey = CKO_SECRET_KEY;
	CK_ATTRIBUTE	template[] = {
				{CKA_CLASS, NULL, sizeof (CK_OBJECT_CLASS)},
				{CKA_KEY_TYPE, NULL, sizeof (CK_KEY_TYPE)},
				{CKA_VALUE, NULL, 0},
				{CKA_TOKEN, NULL, 1},
				{CKA_PRIVATE, NULL, 1},
				{CKA_SENSITIVE, NULL, 1},
				{CKA_EXTRACTABLE, NULL, 1},
				{CKA_ENCRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_DECRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_SIGN, NULL, sizeof (CK_BBOOL)},
				{CKA_VERIFY, NULL, sizeof (CK_BBOOL)},
				{CKA_LABEL, NULL, 0}};
	CK_RV		rv = 0;
	int		attrnum;
	CK_BBOOL	token;
	CK_BBOOL	private;
	CK_BBOOL	sensitive;
	CK_BBOOL	extractable;
	CK_BBOOL	encrypt;
	CK_BBOOL	decrypt;
	CK_BBOOL	sign;
	CK_BBOOL	verify;

	attrnum = (sizeof (template) / sizeof (CK_ATTRIBUTE)) - 1;
	token = (permission & TOKEN) ? 1: 0;
	private = (permission & PRIVATE) ? 1: 0;
	sensitive = (permission & SENSITIVE) ? 1: 0;
	extractable = (permission & EXTRACTABLE) ? 1: 0;
	encrypt = TRUE;
	decrypt = TRUE;
	sign = TRUE;
	verify = TRUE;

	if ((key == NULL) || (keylen == 0)) {
		printf("Need a key.");
		return 1;
	}

	template[0].pValue = &secretKey;
	template[1].pValue = &keytype;
	template[2].pValue = key;
	template[2].ulValueLen = keylen;
	template[3].pValue = &token;
	template[4].pValue = &private;
	template[5].pValue = &sensitive;
	template[6].pValue = &extractable;
	template[7].pValue = &encrypt;
	template[8].pValue = &decrypt;
	template[9].pValue = &sign;
	template[10].pValue = &verify;

	if (label) {
		template[attrnum].pValue = label;
		template[attrnum].ulValueLen = strlen(label);
		attrnum++;
	}

	rv = C_CreateObject(sh, template, attrnum, keyhandle);

	return (rv);
}

int main() 
{ 
	CK_SESSION_HANDLE hSession;
	CK_SLOT_ID slotID;
	CK_RV rv = CKR_OK;
	CK_ULONG slot_count = 0;
	CK_SLOT_ID *pSlotList = NULL;
	CK_UTF8CHAR userPIN[] = {"87654321"};
	CK_OBJECT_HANDLE hKey;
	CK_BYTE encrypt_buf[200];
	CK_ULONG encrypt_buf_len = sizeof (encrypt_buf);
	CK_BYTE decrypt_buf[200];
	CK_ULONG decrypt_buf_len = sizeof (decrypt_buf);
	CK_ULONG len1, len2, len3, len4, len5;

	rv += C_Initialize(NULL);
	rv += C_GetSlotList(CK_FALSE, NULL_PTR, &slot_count);
	if (slot_count == 0) {
		printf("There is no slot.");
		exit(1);
	}
	pSlotList = (CK_SLOT_ID_PTR)malloc(slot_count * sizeof (CK_SLOT_ID));
	assert(pSlotList != NULL);

	rv += C_GetSlotList(CK_FALSE, pSlotList, &slot_count);

	/* 1 : softtoken, when metaslot is disabled */
	rv += C_OpenSession(1, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession);
	assert(rv == CKR_OK);

	rv += C_Login(hSession, CKU_USER, userPIN, sizeof(userPIN));
	assert(rv == CKR_OK);

	rv = create_symmetric_key(hSession, CKK_AES, EXTRACTABLE, &hKey, NULL, aes_ccm_key, sizeof(aes_ccm_key));
	assert(rv == CKR_OK);
#if 0
	CK_MECHANISM mechanism = {CKM_AES_CCM, &aes_ccm_params_enc, sizeof (aes_ccm_params_enc)};
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x", rv);
		return 1;
	}
	rv = C_Encrypt(hSession, aes_ccm_plaintext, sizeof (aes_ccm_plaintext), encrypt_buf, &encrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Encrypt rv = 0x%02x", rv);
		return 1;
	}

	string2hex(encrypt_buf, encrypt_buf_len);
#endif

#if 0
	/* C_Decrypt */
	CK_MECHANISM mech_dec= {CKM_AES_CCM, &aes_ccm_params_dec, sizeof (aes_ccm_params_dec)};
	rv = C_DecryptInit(hSession, &mech_dec, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x", rv);
		return 1;
	}

	rv = C_Decrypt(hSession, aes_ccm_ciphertext, sizeof(aes_ccm_ciphertext), decrypt_buf, &decrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Decrypt rv = 0x%02x", rv);
		return 1;
	}
	string2hex(decrypt_buf, decrypt_buf_len);
#endif
#if 0
	CK_MECHANISM mechanism = {CKM_AES_CCM, &aes_ccm_params_enc, sizeof (aes_ccm_params_enc)};
	len1 = len2 = len3 = len4 = len5 = 200;
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_EncryptUpdate(hSession, aes_ccm_plaintext, 1, encrypt_buf, &len1);
	printf("1. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len1);

	rv = C_EncryptUpdate(hSession, aes_ccm_plaintext + 1, 22, encrypt_buf + len1, &len2);
	printf("2. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len2);

	rv = C_EncryptFinal(hSession, encrypt_buf + len1 + len2, &len3);
	printf("3. C_EncryptFinal rv = 0x%02x, len = %ld\n", rv, len3);

	string2hex(encrypt_buf, len1 + len2 + len3);
#endif
#if 0
	CK_MECHANISM mechanism = {CKM_AES_CCM, &aes_ccm_params_dec, sizeof (aes_ccm_params_dec)};
	len1 = len2 = len3 = len4 = len5 = 200;
	char no_use_buf[10];
	rv = C_DecryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x\n", rv);
		return 1;
	}
	/* expect 0 */
	rv = C_DecryptUpdate(hSession, aes_ccm_ciphertext, 17, no_use_buf, &len1); /* the buf is useless and the len can be everything bug not NULL. :( */
	printf("1. C_DecryptUpdate rv = 0x%02x, len = %ld\n", rv, len1);

	/* expect 0 */
	rv = C_DecryptUpdate(hSession, aes_ccm_ciphertext + 17, 14, no_use_buf, &len2); /* the buf is useless and the len can be everything bug not NULL. :( */
	printf("2. C_DecryptUpdate rv = 0x%02x, len = %ld\n", rv, len2);

	/* expect 23 */
	rv = C_DecryptFinal(hSession, decrypt_buf, &len3); /* this len is the real output length */
	printf("3. C_DecryptFinal rv = 0x%02x, len = %ld\n", rv, len3);

	string2hex(decrypt_buf, len3);
#endif

	(void) C_Finalize(NULL_PTR);

	return (0);
}
