#include <stdio.h> 
#include <string.h> 
#include <assert.h>
#include <security/cryptoki.h>

#define PRIVATE                 0x01
#define TOKEN                   0x02
#define SENSITIVE               0x04
#define EXTRACTABLE             0x08

static uchar_t des_key[] = {
	0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x41, 0x00, 0x00, 0x00, 0x00
};

static uchar_t des_plain[] = {
	0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
	0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46,
	0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e,
};

static uchar_t des_cipher[] = {
	0x7a, 0x6d, 0xe7, 0x5f, 0xd2, 0x2b, 0xb3, 0xd5,
	0x14, 0x1b, 0xb5, 0x36, 0xf6, 0xb1, 0x7e, 0x8a,
	0x4d, 0xbc, 0x04, 0x62, 0xd4, 0x27, 0x2e, 0xb2
};

static void string2hex(unsigned char *pBuf, int num)
{
	int j=0;
	int i;
	for (i = 0; i<num;i++, j++) {
		if (j % 8 == 0)
			printf("\n");
		printf("0x%02x, ", pBuf[i]);
	}
	printf("\n");
	return;
}

static CK_RV create_symmetric_key(CK_SESSION_HANDLE sh, 
		                  CK_KEY_TYPE keytype,
			          uint32_t permission, 
			   	  CK_OBJECT_HANDLE *keyhandle, 
			   	  char *label,
			   	  CK_BYTE *key, 
			   	  CK_ULONG keylen)
{
	CK_OBJECT_CLASS	secretKey = CKO_SECRET_KEY;
	CK_ATTRIBUTE	template[] = {
				{CKA_CLASS, NULL, sizeof (CK_OBJECT_CLASS)},
				{CKA_KEY_TYPE, NULL, sizeof (CK_KEY_TYPE)},
				{CKA_VALUE, NULL, 0},
				{CKA_TOKEN, NULL, 1},
				{CKA_PRIVATE, NULL, 1},
				{CKA_SENSITIVE, NULL, 1},
				{CKA_EXTRACTABLE, NULL, 1},
				{CKA_ENCRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_DECRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_SIGN, NULL, sizeof (CK_BBOOL)},
				{CKA_VERIFY, NULL, sizeof (CK_BBOOL)},
				{CKA_LABEL, NULL, 0}};
	CK_RV		rv = 0;
	int		attrnum;
	CK_BBOOL	token;
	CK_BBOOL	private;
	CK_BBOOL	sensitive;
	CK_BBOOL	extractable;
	CK_BBOOL	encrypt;
	CK_BBOOL	decrypt;
	CK_BBOOL	sign;
	CK_BBOOL	verify;

	attrnum = (sizeof (template) / sizeof (CK_ATTRIBUTE)) - 1;
	token = (permission & TOKEN) ? 1: 0;
	private = (permission & PRIVATE) ? 1: 0;
	sensitive = (permission & SENSITIVE) ? 1: 0;
	extractable = (permission & EXTRACTABLE) ? 1: 0;
	encrypt = TRUE;
	decrypt = TRUE;
	sign = TRUE;
	verify = TRUE;

	if ((key == NULL) || (keylen == 0)) {
		printf("Need a key.");
		return 1;
	}

	template[0].pValue = &secretKey;
	template[1].pValue = &keytype;
	template[2].pValue = key;
	template[2].ulValueLen = keylen;
	template[3].pValue = &token;
	template[4].pValue = &private;
	template[5].pValue = &sensitive;
	template[6].pValue = &extractable;
	template[7].pValue = &encrypt;
	template[8].pValue = &decrypt;
	template[9].pValue = &sign;
	template[10].pValue = &verify;

	if (label) {
		template[attrnum].pValue = label;
		template[attrnum].ulValueLen = strlen(label);
		attrnum++;
	}

	rv = C_CreateObject(sh, template, attrnum, keyhandle);

	return (rv);
}

int main() 
{ 
	CK_SESSION_HANDLE hSession;
	CK_SLOT_ID slotID;
	CK_RV rv = CKR_OK;
	CK_ULONG slot_count = 0;
	CK_SLOT_ID *pSlotList = NULL;
	CK_UTF8CHAR userPIN[] = {"87654321"};
	CK_OBJECT_HANDLE hKey;
	CK_BYTE encrypt_buf[200];
	CK_ULONG encrypt_buf_len = sizeof (encrypt_buf);
	CK_BYTE decrypt_buf[200];
	CK_ULONG decrypt_buf_len = sizeof (decrypt_buf);
	CK_ULONG len1, len2, len3, len4;

	rv += C_Initialize(NULL);
	rv += C_GetSlotList(CK_FALSE, NULL_PTR, &slot_count);
	if (slot_count == 0) {
		printf("There is no slot.");
		exit(1);
	}
	pSlotList = (CK_SLOT_ID_PTR)malloc(slot_count * sizeof (CK_SLOT_ID));
	if (pSlotList == NULL) {
		printf("Allocating slot list memory failed.");	
		exit(1);
	}
	rv += C_GetSlotList(CK_FALSE, pSlotList, &slot_count);
	rv += C_OpenSession(1, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession);
	assert(rv == CKR_OK);

	rv += C_Login(hSession, CKU_USER, userPIN, sizeof(userPIN));
	assert(rv == CKR_OK);

	rv = create_symmetric_key(hSession, CKK_DES2, EXTRACTABLE, &hKey, NULL, des_key, sizeof (des_key));
	assert(rv == CKR_OK);

	CK_MECHANISM mechanism = {CKM_DES3_ECB, NULL, 0};
#if 1
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Encrypt(hSession, des_plain, sizeof (des_plain), encrypt_buf, &encrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Encrypt rv = 0x%02x\n", rv);
		return 1;
	}
	printf("Got cipher :");
	string2hex(encrypt_buf, encrypt_buf_len);
#endif

#if 1
	printf("Got plain:");
	rv = C_DecryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Decrypt(hSession, des_cipher, sizeof(des_cipher), decrypt_buf, &decrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Decrypt rv = 0x%02x\n", rv);
		return 1;
	}
	string2hex(decrypt_buf, decrypt_buf_len);
#endif
	(void) C_Finalize(NULL_PTR);

	return (0);
}
