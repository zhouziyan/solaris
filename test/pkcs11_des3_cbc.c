#include <stdio.h> 
#include <string.h> 
#include <assert.h>
#include <security/cryptoki.h>

#define PRIVATE                 0x01
#define TOKEN                   0x02
#define SENSITIVE               0x04
#define EXTRACTABLE             0x08

static unsigned char des_key[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};

static unsigned char des_iv[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};

static unsigned char des_plain[] = {
	0xa5, 0x17, 0x3a, 0xd5, 0x95, 0x7b, 0x43, 0x70,
	0x7d, 0x2f, 0x31, 0xca, 0xe1, 0xc6, 0x59, 0xf1,
	0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
	0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
};

static unsigned char des_cipher[] = {
	0x7d, 0x35, 0xf8, 0x54, 0x99, 0x82, 0x1b, 0xd6,
	0x16, 0xf7, 0xf0, 0xe6, 0x18, 0xb4, 0xee, 0x2e,
	0x10, 0x8d, 0xda, 0x51, 0xc7, 0x5e, 0x8d, 0xb7,
	0xa9, 0x14, 0x7f, 0xb0, 0x66, 0xfb, 0xd9, 0xa2,
};

static void string2hex(unsigned char *pBuf, int num)
{
	int j=0;
	int i;
	for (i = 0; i<num;i++, j++) {
		if (j % 8 == 0)
			printf("\n");
		printf("0x%02x, ", pBuf[i]);
	}
	printf("\n");
	return;
}

static CK_RV create_symmetric_key(CK_SESSION_HANDLE sh, 
		                  CK_KEY_TYPE keytype,
			          uint32_t permission, 
			   	  CK_OBJECT_HANDLE *keyhandle, 
			   	  char *label,
			   	  CK_BYTE *key, 
			   	  CK_ULONG keylen)
{
	CK_OBJECT_CLASS	secretKey = CKO_SECRET_KEY;
	CK_ATTRIBUTE	template[] = {
				{CKA_CLASS, NULL, sizeof (CK_OBJECT_CLASS)},
				{CKA_KEY_TYPE, NULL, sizeof (CK_KEY_TYPE)},
				{CKA_VALUE, NULL, 0},
				{CKA_TOKEN, NULL, 1},
				{CKA_PRIVATE, NULL, 1},
				{CKA_SENSITIVE, NULL, 1},
				{CKA_EXTRACTABLE, NULL, 1},
				{CKA_ENCRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_DECRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_SIGN, NULL, sizeof (CK_BBOOL)},
				{CKA_VERIFY, NULL, sizeof (CK_BBOOL)},
				{CKA_LABEL, NULL, 0}};
	CK_RV		rv = 0;
	int		attrnum;
	CK_BBOOL	token;
	CK_BBOOL	private;
	CK_BBOOL	sensitive;
	CK_BBOOL	extractable;
	CK_BBOOL	encrypt;
	CK_BBOOL	decrypt;
	CK_BBOOL	sign;
	CK_BBOOL	verify;

	attrnum = (sizeof (template) / sizeof (CK_ATTRIBUTE)) - 1;
	token = (permission & TOKEN) ? 1: 0;
	private = (permission & PRIVATE) ? 1: 0;
	sensitive = (permission & SENSITIVE) ? 1: 0;
	extractable = (permission & EXTRACTABLE) ? 1: 0;
	encrypt = TRUE;
	decrypt = TRUE;
	sign = TRUE;
	verify = TRUE;

	if ((key == NULL) || (keylen == 0)) {
		printf("Need a key.");
		return 222;
	}

	template[0].pValue = &secretKey;
	template[1].pValue = &keytype;
	template[2].pValue = key;
	template[2].ulValueLen = keylen;
	template[3].pValue = &token;
	template[4].pValue = &private;
	template[5].pValue = &sensitive;
	template[6].pValue = &extractable;
	template[7].pValue = &encrypt;
	template[8].pValue = &decrypt;
	template[9].pValue = &sign;
	template[10].pValue = &verify;

	if (label) {
		template[attrnum].pValue = label;
		template[attrnum].ulValueLen = strlen(label);
		attrnum++;
	}

	rv = C_CreateObject(sh, template, attrnum, keyhandle);

	return (rv);
}

int main() 
{ 
	CK_SESSION_HANDLE hSession;
	CK_SLOT_ID slotID;
	CK_RV rv = CKR_OK;
	CK_ULONG slot_count = 0;
	CK_SLOT_ID *pSlotList = NULL;
	CK_UTF8CHAR userPIN[] = {"87654321"};
	CK_OBJECT_HANDLE hKey;
	CK_BYTE encrypt_buf[200];
	CK_ULONG encrypt_buf_len = sizeof (encrypt_buf);
	CK_BYTE decrypt_buf[200];
	CK_ULONG decrypt_buf_len = sizeof (decrypt_buf);
	CK_ULONG len1, len2, len3, len4;

	rv += C_Initialize(NULL);
	rv += C_GetSlotList(CK_FALSE, NULL_PTR, &slot_count);
	if (slot_count == 0) {
		printf("There is no slot.");
		exit(1);
	}
	pSlotList = (CK_SLOT_ID_PTR)malloc(slot_count * sizeof (CK_SLOT_ID));
	if (pSlotList == NULL) {
		printf("Allocating slot list memory failed.");	
		exit(1);
	}
	rv += C_GetSlotList(CK_FALSE, pSlotList, &slot_count);
	rv += C_OpenSession(1, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession);
	assert(rv == CKR_OK);

	rv += C_Login(hSession, CKU_USER, userPIN, sizeof(userPIN));
	assert(rv == CKR_OK);

	rv = create_symmetric_key(hSession, CKK_DES3, EXTRACTABLE, &hKey, NULL, des_key, sizeof (des_key));
	assert(rv == CKR_OK);

	CK_MECHANISM mechanism = {CKM_DES3_CBC, des_iv, sizeof (des_iv)};
#if 0
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Encrypt(hSession, des_plain, sizeof (des_plain), encrypt_buf, &encrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Encrypt rv = 0x%02x\n", rv);
		return 1;
	}
	string2hex(encrypt_buf, encrypt_buf_len);
#endif

#if 0
	rv = C_DecryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Decrypt(hSession, des_cipher, sizeof(des_cipher), decrypt_buf, &decrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Decrypt rv = 0x%02x\n", rv);
		return 1;
	}
	string2hex(decrypt_buf, decrypt_buf_len);
#endif

#if 0
	len1 = len2 = len3 = len4 = 200;
	memset(encrypt_buf, 0, sizeof(encrypt_buf));

	rv = C_EncryptInit(hSession, &mechanism, hKey);
	printf("1. C_EncryptInit rv = 0x%02x\n", rv);

	rv = C_EncryptUpdate(hSession, des_plain + 0, 16, encrypt_buf, &len1);
	printf("2. C_EncryptUpdate rv = 0x%02x, len = %lu\n", rv, len1);

	rv = C_EncryptUpdate(hSession, des_plain + 16, 16, encrypt_buf + len1, &len2);
	printf("3. C_EncryptUpdate rv = 0x%02x, len = %lu\n", rv, len2);

	rv = C_EncryptFinal(hSession, encrypt_buf + len1 + len2, &len3);
	printf("4. C_EncryptFinal rv = 0x%02x, len = %lu\n", rv, len3);

	string2hex(encrypt_buf, len1 + len2 + len3);
#endif

#if 1
	len1 = len2 = len3 = len4 = 200;
	memset(decrypt_buf, 0, sizeof(decrypt_buf));

	rv = C_DecryptInit(hSession, &mechanism, hKey);
	printf("1. C_DecryptInit rv = 0x%02x\n", rv);

	rv = C_DecryptUpdate(hSession, des_cipher, 16, decrypt_buf, &len1);
	printf("2. C_DecryptUpdate rv = 0x%02x, len = %lu\n", rv, len1);

	rv = C_DecryptUpdate(hSession, des_cipher + 16, 16, decrypt_buf + len1, &len2);
	printf("3. C_DecryptUpdate rv = 0x%02x, len = %lu\n", rv, len2);

	rv = C_DecryptFinal(hSession, decrypt_buf + len1 + len2, &len3);
	printf("4. C_DecryptFinal rv = 0x%02x, len = %lu\n", rv, len3);

	string2hex(decrypt_buf, len1 + len2 + len3);
#endif
	(void) C_Finalize(NULL_PTR);

	return (0);
}
