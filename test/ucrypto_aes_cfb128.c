#include <libucrypto.h>

void string2hex(unsigned char *pBuf, int len)
{
	int j=0;
	int i;
	for (i = 0; i < len; i++, j++) {
		if (j % 8 == 0)
		printf("\n");
		printf("0x%02x ", pBuf[i]);
	}
	printf("\n");
}

static unsigned char des_key[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};
static unsigned char des_iv[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};
static unsigned char des_plaintext[] = {
	0xa5, 0x17, 0x3a, 0xd5, 0x95, 0x7b, 0x43, 0x70,
	0xa5, 0x17, 0x3a, 0xd5, 0x95, 0x7b, 0x43, 0x70,
};
static unsigned char des_ciphertext[] = {
	0xa5, 0x17, 0x3a, 0xd5, 0x95, 0x7b, 0x43, 0x70,
	0x7d, 0x2f, 0x31, 0xca, 0xe1, 0xc6, 0x59, 0xf1
};

int
main(void)
{
	crypto_ctx_t ctx;
	int rv = 0;
	unsigned char e[100];
#if 0
	size_t el = sizeof (e);
	rv = ucrypto_encrypt(CRYPTO_AES_CFB128, des_key, sizeof (des_key), des_iv, sizeof (des_iv), des_plaintext, sizeof (des_plaintext), e, &el);
	printf("rv: %s, length = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	size_t e1 = e2 = e3 = 100;
	rv = ucrypto_encrypt_init(&ctx, CRYPTO_AES_CFB128, des_key, sizeof (des_key), des_iv, sizeof (des_iv));
	printf("init rv: %s\n",ucrypto_strerror(rv));
	rv = ucrypto_encrypt_update(&ctx, des_plaintext, 8, e, &e1);
	printf("update rv: %s, el = %d\n",ucrypto_strerror(rv), e1);
	rv = ucrypto_encrypt_update(&ctx, des_plaintext + 8, 8, e, &e2);
	printf("update rv: %s, el = %d\n",ucrypto_strerror(rv), e2);
	rv = ucrypto_encrypt_final(&ctx, e + e1 + e2, &e3);
	printf("final rv: %s, el = %d\n",ucrypto_strerror(rv), e3);
	string2hex(e, e1+e2+e3);
#endif

	return 0;
}
