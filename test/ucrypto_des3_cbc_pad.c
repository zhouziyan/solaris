#include <libucrypto.h>

void string2hex(unsigned char *pBuf, int len)
{
	int j=0;
	int i;
	for (i = 0; i < len; i++, j++) {
		if (j % 8 == 0)
		printf("\n");
		printf("0x%02x ", pBuf[i]);
	}
	printf("\n");
}

static unsigned char des_key[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};
static unsigned char des_iv[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
};
static unsigned char des_plain[] = {
	0xa5, 0x17, 0x3a, 0xd5, 0x95, 0x7b, 0x43, 0x70,
	0x7d, 0x2f, 0x31, 0xca, 0xe1, 0xc6, 0x59, 0xf1,
	0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
};
static unsigned char des_cipher[] = {
	0x7d, 0x35, 0xf8, 0x54, 0x99, 0x82, 0x1b, 0xd6,
	0x16, 0xf7, 0xf0, 0xe6, 0x18, 0xb4, 0xee, 0x2e,
	0x10, 0x8d, 0xda, 0x51, 0xc7, 0x5e, 0x8d, 0xb7,
	0xa9, 0x14, 0x7f, 0xb0, 0x66, 0xfb, 0xd9, 0xa2,
};

int
main(void)
{
	crypto_ctx_t ctx;
	int rv = 0;
	unsigned char e[100];
	size_t el, e1, e2, e3;
#if 0
	el = sizeof (e);
	rv = ucrypto_encrypt(CRYPTO_DES3_CBC_PAD, des_key, sizeof (des_key), des_iv, sizeof (des_iv), des_plain, sizeof (des_plain), e, &el);
	printf("rv: %s, length = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	el = sizeof (e);
	rv = ucrypto_decrypt(CRYPTO_DES3_CBC_PAD, des_key, sizeof (des_key), des_iv, sizeof (des_iv), des_cipher, sizeof (des_cipher), e, &el);
	printf("rv: %s, length = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	e1 = e2 = e3 = 100;
	rv = ucrypto_encrypt_init(&ctx, CRYPTO_DES3_CBC_PAD, des_key, sizeof (des_key), des_iv, sizeof (des_iv));
	printf("1. init rv: %s\n",ucrypto_strerror(rv));

	rv = ucrypto_encrypt_update(&ctx, des_plain, 16, e, &e1);
	printf("2. update rv: %s, len = %d\n",ucrypto_strerror(rv), e1);

	rv = ucrypto_encrypt_update(&ctx, des_plain + 16, 8, e + e1, &e2);
	printf("3. update rv: %s, len = %d\n",ucrypto_strerror(rv), e2);

	rv = ucrypto_encrypt_final(&ctx, e + e1 + e2, &e3);
	printf("4. final rv: %s, len = %d\n",ucrypto_strerror(rv), e3);
	string2hex(e, e1 + e2 + e3);
#endif

#if 0
	e1 = e2 = e3 = 100;
	rv = ucrypto_decrypt_init(&ctx, CRYPTO_DES3_CBC_PAD, des_key, sizeof (des_key), des_iv, sizeof (des_iv));
	printf("1. init rv: %s\n",ucrypto_strerror(rv));

	rv = ucrypto_decrypt_update(&ctx, des_cipher, 16, e, &e1);
	printf("2. update rv: %s, len = %d\n",ucrypto_strerror(rv), e1);

	rv = ucrypto_decrypt_update(&ctx, des_cipher + 16, 16, e + e1, &e2);
	printf("3. update rv: %s, len = %d\n",ucrypto_strerror(rv), e2);

	rv = ucrypto_decrypt_final(&ctx, e + e1 + e2, &e3);
	printf("4. final rv: %s, len = %d\n",ucrypto_strerror(rv), e3);
	string2hex(e, e1 + e2 +e3);
#endif

	return 0;
}
