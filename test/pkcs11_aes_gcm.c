#include <stdio.h> 
#include <string.h> 
#include <assert.h>
#include <security/cryptoki.h>

#define PRIVATE                 0x01
#define TOKEN                   0x02
#define SENSITIVE               0x04
#define EXTRACTABLE             0x08

static unsigned char aes_gcm_key[] = {
	0xfe, 0xff, 0xe9, 0x92, 0x86, 0x65, 0x73, 0x1c,
	0x6d, 0x6a, 0x8f, 0x94, 0x67, 0x30, 0x83, 0x08
};

static unsigned char aes_gcm_iv[] = {
	0xca, 0xfe, 0xba, 0xbe, 0xfa, 0xce, 0xdb, 0xad,
	0xde, 0xca, 0xf8, 0x88
};

static unsigned char aes_gcm_adata[] = {
	0xfe, 0xed, 0xfa, 0xce, 0xde, 0xad, 0xbe, 0xef,
	0xfe, 0xed, 0xfa, 0xce, 0xde, 0xad, 0xbe, 0xef,
	0xab, 0xad, 0xda, 0xd2
};

static unsigned char aes_gcm_plaintext[] = {
	0xd9, 0x31, 0x32, 0x25, 0xf8, 0x84, 0x06, 0xe5,
	0xa5, 0x59, 0x09, 0xc5, 0xaf, 0xf5, 0x26, 0x9a,
	0x86, 0xa7, 0xa9, 0x53, 0x15, 0x34, 0xf7, 0xda,
	0x2e, 0x4c, 0x30, 0x3d, 0x8a, 0x31, 0x8a, 0x72,
	0x1c, 0x3c, 0x0c, 0x95, 0x95, 0x68, 0x09, 0x53,
	0x2f, 0xcf, 0x0e, 0x24, 0x49, 0xa6, 0xb5, 0x25,
	0xb1, 0x6a, 0xed, 0xf5, 0xaa, 0x0d, 0xe6, 0x57,
	0xba, 0x63, 0x7b, 0x39
};

static unsigned char aes_gcm_ciphertext[] = {
	0x42, 0x83, 0x1e, 0xc2, 0x21, 0x77, 0x74, 0x24,
	0x4b, 0x72, 0x21, 0xb7, 0x84, 0xd0, 0xd4, 0x9c,
	0xe3, 0xaa, 0x21, 0x2f, 0x2c, 0x02, 0xa4, 0xe0,
	0x35, 0xc1, 0x7e, 0x23, 0x29, 0xac, 0xa1, 0x2e,
	0x21, 0xd5, 0x14, 0xb2, 0x54, 0x66, 0x93, 0x1c,
	0x7d, 0x8f, 0x6a, 0x5a, 0xac, 0x84, 0xaa, 0x05,
	0x1b, 0xa3, 0x0b, 0x39, 0x6a, 0x0a, 0xac, 0x97,
	0x3d, 0x58, 0xe0, 0x91, 0x5b, 0xc9, 0x4f, 0xbc,
	0x32, 0x21, 0xa5, 0xdb, 0x94, 0xfa, 0xe9, 0x5a,
	0xe7, 0x12, 0x1a, 0x47,
};

CK_GCM_PARAMS aes_gcm_params = {
	.pIv = aes_gcm_iv,
	.ulIvLen = sizeof (aes_gcm_iv),
	.pAAD = aes_gcm_adata,
	.ulAADLen = sizeof (aes_gcm_adata),
	.ulTagBits = 128
};

static void string2hex(unsigned char *pBuf, int num)
{
	int j=0;
	int i;
	for (i = 0; i<num;i++, j++) {
		if (j % 8 == 0)
			printf("\n");
		printf("0x%02x ", pBuf[i]);
	}
	printf("\n");
	return;
}

static CK_RV create_symmetric_key(CK_SESSION_HANDLE sh, 
		                  CK_KEY_TYPE keytype,
			          uint32_t permission, 
			   	  CK_OBJECT_HANDLE *keyhandle, 
			   	  char *label,
			   	  CK_BYTE *key, 
			   	  CK_ULONG keylen) {

	CK_OBJECT_CLASS	secretKey = CKO_SECRET_KEY;
	CK_ATTRIBUTE	template[] = {
				{CKA_CLASS, NULL, sizeof (CK_OBJECT_CLASS)},
				{CKA_KEY_TYPE, NULL, sizeof (CK_KEY_TYPE)},
				{CKA_VALUE, NULL, 0},
				{CKA_TOKEN, NULL, 1},
				{CKA_PRIVATE, NULL, 1},
				{CKA_SENSITIVE, NULL, 1},
				{CKA_EXTRACTABLE, NULL, 1},
				{CKA_ENCRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_DECRYPT, NULL, sizeof (CK_BBOOL)},
				{CKA_SIGN, NULL, sizeof (CK_BBOOL)},
				{CKA_VERIFY, NULL, sizeof (CK_BBOOL)},
				{CKA_LABEL, NULL, 0}};
	CK_RV		rv = 0;
	int		attrnum;
	CK_BBOOL	token;
	CK_BBOOL	private;
	CK_BBOOL	sensitive;
	CK_BBOOL	extractable;
	CK_BBOOL	encrypt;
	CK_BBOOL	decrypt;
	CK_BBOOL	sign;
	CK_BBOOL	verify;

	attrnum = (sizeof (template) / sizeof (CK_ATTRIBUTE)) - 1;
	token = (permission & TOKEN) ? 1: 0;
	private = (permission & PRIVATE) ? 1: 0;
	sensitive = (permission & SENSITIVE) ? 1: 0;
	extractable = (permission & EXTRACTABLE) ? 1: 0;
	encrypt = TRUE;
	decrypt = TRUE;
	sign = TRUE;
	verify = TRUE;

	if ((key == NULL) || (keylen == 0)) {
		printf("Need a key.");
		return 222;
	}

	template[0].pValue = &secretKey;
	template[1].pValue = &keytype;
	template[2].pValue = key;
	template[2].ulValueLen = keylen;
	template[3].pValue = &token;
	template[4].pValue = &private;
	template[5].pValue = &sensitive;
	template[6].pValue = &extractable;
	template[7].pValue = &encrypt;
	template[8].pValue = &decrypt;
	template[9].pValue = &sign;
	template[10].pValue = &verify;

	if (label) {
		template[attrnum].pValue = label;
		template[attrnum].ulValueLen = strlen(label);
		attrnum++;
	}

	rv = C_CreateObject(sh, template, attrnum, keyhandle);

	return (rv);
}

int main() 
{ 
	CK_SESSION_HANDLE hSession;
	CK_SLOT_ID slotID;
	CK_RV rv = CKR_OK;
	CK_ULONG slot_count = 0;
	CK_SLOT_ID *pSlotList = NULL;
	CK_UTF8CHAR userPIN[] = {"87654321"};
	CK_OBJECT_HANDLE hKey;
	CK_BYTE encrypt_buf[200];
	CK_BYTE decrypt_buf[200];

	/* length for C_Encrypt & C_Decrypt */
	CK_ULONG encrypt_buf_len = sizeof (encrypt_buf);
	CK_ULONG decrypt_buf_len = sizeof (decrypt_buf);

	/* length for C_EncryptUpdate & C_DecryptUpdate */
	CK_ULONG len1, len2, len3, len4, len5;

	rv += C_Initialize(NULL);
	rv += C_GetSlotList(CK_FALSE, NULL_PTR, &slot_count);
	if (slot_count == 0) {
		printf("There is no slot.");
		exit(1);
	}
	pSlotList = (CK_SLOT_ID_PTR)malloc(slot_count * sizeof (CK_SLOT_ID));
	if (pSlotList == NULL) {
		printf("Allocating slot list memory failed.");	
		exit(1);
	}
	rv += C_GetSlotList(CK_FALSE, pSlotList, &slot_count);

	/* slot ID 1 is softtoken when metaslot is disabled */
	rv += C_OpenSession(1, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &hSession);
	assert(rv == CKR_OK);

	rv += C_Login(hSession, CKU_USER, userPIN, sizeof(userPIN));
	assert(rv == CKR_OK);

	/* 5. Generate a key */
	rv = create_symmetric_key(hSession, CKK_AES, EXTRACTABLE, &hKey, NULL, aes_gcm_key, sizeof(aes_gcm_key));
	assert(rv == CKR_OK);

	CK_MECHANISM mechanism = {CKM_AES_GCM, &aes_gcm_params, sizeof (aes_gcm_params)};
#if 0
	/* 6. Test C_Encrypt */
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Encrypt(hSession, aes_gcm_plaintext, sizeof (aes_gcm_plaintext), encrypt_buf, &encrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Encrypt rv = 0x%02x\n", rv);
		return 1;
	}
	string2hex(encrypt_buf, encrypt_buf_len);
#endif
#if 0
	/* 7. Test C_Decrypt */
	rv = C_DecryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_Decrypt(hSession, aes_gcm_ciphertext, sizeof(aes_gcm_ciphertext), decrypt_buf, &decrypt_buf_len);
	if(rv != CKR_OK) {
		printf("C_Decrypt rv = 0x%02x\n", rv);
		return 1;
	}
	string2hex(decrypt_buf, decrypt_buf_len);
#endif
#if 0
	len1 = len2 = len3 = len4 = len5 = 200;
	rv = C_EncryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_EncryptInit rv = 0x%02x\n", rv);
		return 1;
	}

	rv = C_EncryptUpdate(hSession, aes_gcm_plaintext + 0, 16, encrypt_buf, &len1);
	printf("1. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len1);

	rv = C_EncryptUpdate(hSession, aes_gcm_plaintext + 16, 17, encrypt_buf + len1, &len2);
	printf("2. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len2);

	rv = C_EncryptUpdate(hSession, aes_gcm_plaintext + 33, 15, encrypt_buf + len1 + len2, &len3);
	printf("3. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len3);

	rv = C_EncryptUpdate(hSession, aes_gcm_plaintext + 48, 12, encrypt_buf + len1 + len2 + len3, &len4);
	printf("4. C_EncryptUpdate rv = 0x%02x, len = %ld\n", rv, len4);

	rv = C_EncryptFinal(hSession, encrypt_buf + len1 + len2 + len3 + len4, &len5);
	printf("5. C_EncryptFinal rv = 0x%02x, len = %ld\n", rv, len5);

	string2hex(encrypt_buf, len1 + len2 + len3 + len4 + len5);
#endif
#if 0
	len1 = len2 = len3 = len4 = len5 = 200;
	char no_use_buf[10];
	rv = C_DecryptInit(hSession, &mechanism, hKey);
	if(rv != CKR_OK) {
		printf("C_DecryptInit rv = 0x%02x\n", rv);
		return 1;
	}
	/* expect 0 */
	rv = C_DecryptUpdate(hSession, aes_gcm_ciphertext, 60, no_use_buf, &len1); /* the buf is useless and the len can be everything bug not NULL. :( */
	printf("1. C_DecryptUpdate rv = 0x%02x, len = %ld\n", rv, len1);

	/* expect 0 */
	rv = C_DecryptUpdate(hSession, aes_gcm_ciphertext + 60, 16, no_use_buf, &len2); /* the buf is useless and the len can be everything bug not NULL. :( */
	printf("2. C_DecryptUpdate rv = 0x%02x, len = %ld\n", rv, len2);

	/* expect 60 */
	rv = C_DecryptFinal(hSession, decrypt_buf, &len3); /* this len is the real output length */
	printf("3. C_DecryptFinal rv = 0x%02x, len = %ld\n", rv, len3);

	string2hex(decrypt_buf, len3);
#endif
	rv = C_Finalize(NULL_PTR);

	return (rv);
}
