#include <libucrypto.h>

void string2hex(unsigned char *pBuf, int num)
{
	int j=0;
	int i;
	for (i = 0; i<num;i++, j++) {
	if (j % 8 == 0)
		printf("\n");
		printf("0x%02x, ", pBuf[i]);
	}
	printf("\n");
	return;
}
static unsigned char aes_key[] = {
	0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
	0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c
};


static unsigned char aes_iv[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
};

static unsigned char aes_plain[] = {
	0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96,
	0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a,
};
static unsigned char aes_cipher[] = {
	0x76, 0x49, 0xab, 0xac, 0x81, 0x19, 0xb2, 0x46,
	0xce, 0xe9, 0x8e, 0x9b, 0x12, 0xe9, 0x19, 0x7d,
	0x89, 0x64, 0xe0, 0xb1, 0x49, 0xc1, 0x0b, 0x7b,
	0x68, 0x2e, 0x6e, 0x39, 0xaa, 0xeb, 0x73, 0x1c,
};

int main()
{
	crypto_ctx_t ctx;
	unsigned char e[100];
	size_t el, e1, e2;
	int rv = 0;
#if 1
	el = sizeof (e);
	rv = ucrypto_encrypt(CRYPTO_AES_CBC_PAD, aes_key, sizeof (aes_key), aes_iv, sizeof (aes_iv), aes_plain, sizeof (aes_plain), e, &el);
	printf("rv = %x, el = %d\n",rv, el);
	string2hex(e, el);
#endif

#if 0
	el = sizeof (e);
	rv = ucrypto_decrypt(CRYPTO_AES_CBC_PAD, aes_key, sizeof (aes_key), aes_iv, sizeof (aes_iv), aes_cipher, sizeof (aes_cipher), e, &el);
	printf("rv = %s, el = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	e1 = e2 = 100;
	rv = ucrypto_encrypt_init(&ctx, CRYPTO_AES_CBC_PAD, aes_key, sizeof (aes_key), aes_iv, sizeof (aes_iv));
	printf("rv = %x\n",rv);
	rv = ucrypto_encrypt_update(&ctx, aes_plain, sizeof (aes_plain), e, &e1);
	printf("rv = %x, el = %d\n",rv, e1);
	string2hex(e, e1);
	rv = ucrypto_encrypt_final(&ctx, e + e1, &e2);
	printf("rv = %x, el = %d\n",rv, e2);
	string2hex(e, e1 + e2);
#endif

#if 0
	e1 = e2 = 100;
	rv = ucrypto_decrypt_init(&ctx, CRYPTO_AES_CBC_PAD, aes_key, sizeof (aes_key), aes_iv, sizeof (aes_iv));
	printf("rv = %x\n",rv);
	rv = ucrypto_decrypt_update(&ctx, aes_cipher, sizeof (aes_cipher), e, &e1);
	printf("rv = %x, el = %d\n",rv, e1);
	rv = ucrypto_decrypt_final(&ctx, e + e1, &e2);
	printf("rv = %x, el = %d\n",rv, e2);
	string2hex(e, e1+e2);
#endif

	return 0;
}
