#include <libucrypto.h>

void string2hex(unsigned char *pBuf, int len)
{
	int j=0;
	int i;
	for (i = 0; i < len; i++, j++) {
		if (j % 8 == 0)
		printf("\n");
		printf("0x%02x ", pBuf[i]);
	}
	printf("\n");
}
static unsigned char camellia_key[] = {
	0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
	0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c
};

static unsigned char camellia_iv[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
};

static unsigned char camellia_plain[] = {
	0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96,
	0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a,
	0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
	0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
};

static unsigned char camellia_cipher[] = {
	0x16, 0x07, 0xcf, 0x49, 0x4b, 0x36, 0xbb, 0xf0,
	0x0d, 0xae, 0xb0, 0xb5, 0x03, 0xc8, 0x31, 0xab,
	0xf5, 0x75, 0x7e, 0xbe, 0x82, 0xa8, 0xcf, 0xb6,
	0x04, 0xbc, 0x96, 0xbe, 0x1c, 0xdb, 0xd0, 0x50
};

int
main(void)
{

	crypto_ctx_t ctx;
	int rv = 0;
	unsigned char e[100];
	size_t el, e1, e2, e3;
#if 0
	el = sizeof (e);
	rv = ucrypto_encrypt(CRYPTO_CAMELLIA_CBC, camellia_key, sizeof (camellia_key), camellia_iv, sizeof (camellia_iv), camellia_plain, sizeof (camellia_plain), e, &el);
	printf("rv: %s, length = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	el = sizeof (e);
	rv = ucrypto_decrypt(CRYPTO_CAMELLIA_CBC, camellia_key, sizeof (camellia_key), camellia_iv, sizeof (camellia_iv), camellia_cipher, sizeof (camellia_cipher), e, &el);
	printf("rv: %s, length = %d\n",ucrypto_strerror(rv), el);
	string2hex(e, el);
#endif

#if 0
	e1 = e2 = e3 = 100;
	rv = ucrypto_encrypt_init(&ctx, CRYPTO_CAMELLIA_CBC, camellia_key, sizeof (camellia_key), camellia_iv, sizeof (camellia_iv));
	printf("init rv: %s\n",ucrypto_strerror(rv));

	rv = ucrypto_encrypt_update(&ctx, camellia_plain, 16, e, &e1);
	printf("update rv: %s, el = %d\n",ucrypto_strerror(rv), e1);

	rv = ucrypto_encrypt_final(&ctx, e + e1, &e2);
	printf("final rv: %s, el = %d\n",ucrypto_strerror(rv), e2);
	string2hex(e, e1 + e2);
#endif

#if 0
	e1 = e2 = 100;
	rv = ucrypto_decrypt_init(&ctx, CRYPTO_CAMELLIA_CBC, camellia_key, sizeof (camellia_key), camellia_iv, sizeof (camellia_iv));
	printf("init rv: %s\n",ucrypto_strerror(rv));
	rv = ucrypto_decrypt_update(&ctx, camellia_cipher, sizeof (camellia_cipher), e, &e1);
	printf("update rv: %s, el = %d\n",ucrypto_strerror(rv), e1);
	rv = ucrypto_decrypt_final(&ctx, e + e1, &e2);
	printf("final rv: %s, el = %d\n",ucrypto_strerror(rv), e2);
	string2hex(e, e1+e2);
#endif

	return 0;
}
