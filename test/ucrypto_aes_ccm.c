#include <libucrypto.h>

void string2hex(unsigned char *pBuf, int num)
{
	int j=0;
	int i;
	for (i = 0; i<num;i++, j++) {
	if (j % 8 == 0)
		printf("\n");
		printf("0x%02x, ", pBuf[i]);
	}
	printf("\n");
	return;
}

static unsigned char aes_ccm_key[] = {
	0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
	0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf
};

static unsigned char aes_ccm_nonce[] = {
	0x00, 0x00, 0x00, 0x03, 0x02, 0x01, 0x00, 0xa0,
	0xa1, 0xa2, 0xa3, 0xa4, 0xa5
};

static unsigned char aes_ccm_adata[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07
};

static unsigned char aes_ccm_plain[] = {
	0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
	0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
	0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e
};

static unsigned char aes_ccm_cipher[] = {
	0x58, 0x8c, 0x97, 0x9a, 0x61, 0xc6, 0x63, 0xd2,
	0xf0, 0x66, 0xd0, 0xc2, 0xc0, 0xf9, 0x89, 0x80,
	0x6d, 0x5f, 0x6b, 0x61, 0xda, 0xc3, 0x84, 0x17,
	0xe8, 0xd1, 0x2c, 0xfd, 0xf9, 0x26, 0xe0
};

crypto_ccm_params_t aes_ccm_params_enc = {
	.cc_data_size = sizeof (aes_ccm_plain), /* used for plaintext or ciphertext */
	.cc_mac_size = 8,
	.cc_nonce = aes_ccm_nonce,
	.cc_nonce_size = sizeof (aes_ccm_nonce),
	.cc_auth_data = aes_ccm_adata,
	.cc_auth_data_size = sizeof (aes_ccm_adata)
};

crypto_ccm_params_t aes_ccm_params_dec = {
	.cc_data_size = sizeof (aes_ccm_cipher), /* used for plaintext or ciphertext */
	.cc_mac_size = 8,
	.cc_nonce = aes_ccm_nonce,
	.cc_nonce_size = sizeof (aes_ccm_nonce),
	.cc_auth_data = aes_ccm_adata,
	.cc_auth_data_size = sizeof (aes_ccm_adata)
};

int main()
{
	int rv = 0;
	crypto_ctx_t ctx;
	unsigned char e[100];
	size_t el;
	int i;
	size_t e1, e2, e3;
#if 1
	el = sizeof (e);
	rv = ucrypto_encrypt(CRYPTO_AES_CCM, aes_ccm_key, sizeof (aes_ccm_key), &aes_ccm_params_enc, sizeof (aes_ccm_params_enc), aes_ccm_plain, sizeof (aes_ccm_plain), e, &el);
	/* expected length is sizeof (aes_ccm_plain) + tag in bytes */
	printf("rv = %x, el = %d\n",rv, el);
	string2hex(e, el);
#endif
#if 1
	el = sizeof (e);
	rv = ucrypto_decrypt(CRYPTO_AES_CCM, aes_ccm_key, sizeof (aes_ccm_key), &aes_ccm_params_dec, sizeof (aes_ccm_params_dec), aes_ccm_cipher, sizeof (aes_ccm_cipher), e, &el);
	printf("rv = %x, el = %d\n",rv, el);
	string2hex(e, el);
#endif
#if 1
	e1 = e2 = e3 = 100;
	rv = ucrypto_encrypt_init(&ctx, CRYPTO_AES_CCM, aes_ccm_key, sizeof (aes_ccm_key), &aes_ccm_params_enc, sizeof (aes_ccm_params_enc));
	printf("init, rv = %x\n",rv);

	rv = ucrypto_encrypt_update(&ctx, aes_ccm_plain, 1, e, &e1);
	printf("update, rv = %x, len = %d\n",rv, e1);

	rv = ucrypto_encrypt_update(&ctx, aes_ccm_plain + 1, 22, e + e1, &e2);
	printf("update, rv = %x, len = %d\n",rv, e2);

	rv = ucrypto_encrypt_final(&ctx, e + e1 + e2, &e3);
	printf("final, rv= %x, len = %d\n",rv, e3);

	string2hex(e, e1 + e2 + e3);
#endif
#if 1
	e1 = e2 = e3 = 100;
	rv = ucrypto_decrypt_init(&ctx, CRYPTO_AES_CCM, aes_ccm_key, sizeof (aes_ccm_key), &aes_ccm_params_dec, sizeof (aes_ccm_params_dec));
	printf("init, rv = %x\n",rv);

	/* expect 0 */
	rv = ucrypto_decrypt_update(&ctx, aes_ccm_cipher, 30, e, &e1);
	printf("update, rv = %x, len = %d\n",rv, e1);

	/* expect 0 */
	rv = ucrypto_decrypt_update(&ctx, aes_ccm_cipher + 30, 1, e + e1, &e2);
	printf("update, rv = %x, len = %d\n",rv, e2);

	/* only final could get real length */
	rv = ucrypto_decrypt_final(&ctx, e + e1 +e2, &e3);
	printf("updata, rv = %x, len = %d\n",rv, e3);

	string2hex(e, e1 + e2 + e3);
#endif

	return 0;
}
