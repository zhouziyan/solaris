#include <sys/modctl.h>
#include <sys/conf.h>
#include <sys/devops.h>

#include <sys/stat.h>
#include <sys/conf.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>

#include <sys/types.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <sys/open.h>
#include <sys/cred.h>
#include <sys/disp.h>
#include <sys/cpuvar.h>
#include <sys/callb.h>
#include <sys/uio.h>
#include <sys/kobj.h>
#include <sys/crypto/common.h>
#include <sys/crypto/api.h>
#include <sys/crypto/ioctl.h>

#include <sys/sha3.h>

#define	BITSINBYTE	8

/*
 * Module linkage information for the kernel.
 */
static struct modlmisc modlmisc = {
        &mod_miscops,
        "SHA3 test"
};

static struct modlinkage modlinkage = {
        MODREV_1, (void *)&modlmisc, NULL
};

struct tstalign {
	kmutex_t tlock;
	uint8_t	 flag;
	uchar_t	fill[64 - sizeof(kmutex_t) - sizeof(uint8_t)];
};


int errors = 0;
boolean_t silent = B_FALSE;


static int
sha3test_digest_init(crypto_ctx_template_t *ctx, char *mech)
{
	int		rv;
	crypto_mechanism_t mechanism;

	mechanism.cm_type = crypto_mech2id(mech);
	mechanism.cm_param = NULL;
	mechanism.cm_param_len = 0;
	
	rv = crypto_digest_init(&mechanism, ctx, NULL);

	return (rv);
}


static int
sha3test_digest_update(crypto_ctx_template_t *ctx,
    uchar_t *data, size_t data_len)
{
	int		rv;
	crypto_data_t	in;

	in.cd_format = CRYPTO_DATA_RAW;
	in.cd_raw.iov_base = (char *)data;
	in.cd_raw.iov_len = data_len;
	in.cd_offset = 0;
	in.cd_length = data_len;
	in.cd_miscdata = NULL;

	rv = crypto_digest_update(*ctx, &in, NULL);

	return (rv);
}


static int
sha3test_digest_final(crypto_ctx_template_t *ctx,
    uchar_t *digest, size_t *digest_len)
{
	int		rv;
	crypto_data_t	out;

	out.cd_format = CRYPTO_DATA_RAW;
	out.cd_raw.iov_base = (char *)digest;
	out.cd_raw.iov_len = *digest_len;
	out.cd_offset = 0;
	out.cd_length = *digest_len;
	out.cd_miscdata = NULL;

	rv = crypto_digest_final(*ctx, &out, NULL);
	*digest_len = out.cd_length;

	return (rv);
}

static int
sha3test_multipart_digest(char *mech, uchar_t *data, size_t data_len,
    uchar_t *exp_digest, size_t exp_digest_len)
{
	uchar_t			digest_buf[SHA3_MAX_DIGEST_LENGTH];
	size_t			digest_len = SHA3_MAX_DIGEST_LENGTH;
	size_t			rem = data_len, len;
	size_t			partsize = 5;
	crypto_ctx_template_t	ctx;
	int			rv;

	rv = sha3test_digest_init(&ctx, mech);

	if (rv != CRYPTO_SUCCESS) {
		printf("sha3test_multipart_digest:"
		    " crypto_digest_init failed: rv = 0x%x\n", rv);
		return (rv);
	}

	while (rem > 0) {
		len = ((rem > partsize) ? partsize : rem);

		rv = sha3test_digest_update(&ctx, data + data_len - rem, len);

		if (rv != CRYPTO_SUCCESS) {
			printf("sha3test_multipart_digest:"
			    " crypto_digest_update failed: rv = 0x%x\n", rv);
			return (rv);
		}

		rem -= len;
	}

	rv = sha3test_digest_final(&ctx, digest_buf, &digest_len);

	if (rv != CRYPTO_SUCCESS) {
		printf("sha3test_multipart_digest:"
		    " crypto_digest_final failed: rv = 0x%x\n", rv);
		return (rv);
	}

	if (exp_digest_len != digest_len) {
		printf("sha3test_multipart_digest: unexpected digest length\n");
		printf("mech = %s exp_digest_len = %d, got = %d\n",
		    mech, exp_digest_len, digest_len);
		return (CRYPTO_FAILED);
	}

	if (memcmp(exp_digest, digest_buf, exp_digest_len) != 0) {
		printf("sha3test_multipart_digest: (mech = %s)"
		    " unexpected digest\n", mech);

		return (CRYPTO_FAILED);
	}

	return (CRYPTO_SUCCESS);
}


static int
sha3test_digest(char *mech, uchar_t *data, size_t data_len,
    uchar_t *exp_digest, size_t exp_digest_len)
{
	int		rv;
	crypto_mechanism_t mechanism;
	crypto_data_t in, out;
	crypto_key_t crypto_key;
	uchar_t digest_buf[SHA3_MAX_DIGEST_LENGTH];
	
	printf("Tesing mech = %s \n", mech);
	mechanism.cm_type = crypto_mech2id(mech);
	mechanism.cm_param = NULL;
	mechanism.cm_param_len = 0;

	in.cd_format = CRYPTO_DATA_RAW;
	in.cd_raw.iov_base = (char *)data;
	in.cd_raw.iov_len = data_len;
	in.cd_offset = 0;
	in.cd_length = data_len;
	in.cd_miscdata = NULL;

	out.cd_format = CRYPTO_DATA_RAW;
	out.cd_raw.iov_base = (char *)digest_buf;
	out.cd_raw.iov_len = SHA3_MAX_DIGEST_LENGTH;
	out.cd_offset = 0;
	out.cd_length = SHA3_MAX_DIGEST_LENGTH;
	out.cd_miscdata = NULL;
	
	rv = crypto_digest(&mechanism, &in, &out, NULL);

	if (rv != CRYPTO_SUCCESS) {
		printf("sha3test_digest: crypto_digest failed: rv = 0x%x\n",
		    rv);
		return (rv);
	}

	if (exp_digest_len != out.cd_length) {
		printf("sha3test_digest: unexpected digest length\n");
		printf("mech = %s exp_digest_len = %d, out.cd_length = %d\n",
		    mech, exp_digest_len, out.cd_length);
		return (CRYPTO_FAILED);
	}

	if (memcmp(exp_digest, digest_buf, exp_digest_len) != 0) {
		printf("sha3test_digest: (mech = %s) unexpected digest\n",
		    mech);

		return (CRYPTO_FAILED);
	}

	return (CRYPTO_SUCCESS);
}

char *input_a_u = "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn"
	"hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu";

uchar_t output_a_u_224[] = {
	0x54, 0x3e, 0x68, 0x68, 0xe1, 0x66, 0x6c, 0x1a,
	0x64, 0x36, 0x30, 0xdf, 0x77, 0x36, 0x7a, 0xe5,
	0xa6, 0x2a, 0x85, 0x07, 0x0a, 0x51, 0xc1, 0x4c,
	0xbf, 0x66, 0x5c, 0xbc
};
uchar_t output_a_u_256[] = {
	0x91, 0x6f, 0x60, 0x61, 0xfe, 0x87, 0x97, 0x41,
	0xca, 0x64, 0x69, 0xb4, 0x39, 0x71, 0xdf, 0xdb,
	0x28, 0xb1, 0xa3, 0x2d, 0xc3, 0x6c, 0xb3, 0x25,
	0x4e, 0x81, 0x2b, 0xe2, 0x7a, 0xad, 0x1d, 0x18
};

uchar_t output_a_u_384[] = {
	0x79, 0x40, 0x7d, 0x3b, 0x59, 0x16, 0xb5, 0x9c,
	0x3e, 0x30, 0xb0, 0x98, 0x22, 0x97, 0x47, 0x91,
	0xc3, 0x13, 0xfb, 0x9e, 0xcc, 0x84, 0x9e, 0x40,
	0x6f, 0x23, 0x59, 0x2d, 0x04, 0xf6, 0x25, 0xdc,
	0x8c, 0x70, 0x9b, 0x98, 0xb4, 0x3b, 0x38, 0x52,
	0xb3, 0x37, 0x21, 0x61, 0x79, 0xaa, 0x7f, 0xc7
};

uchar_t output_a_u_512[] = {
	0xaf, 0xeb, 0xb2, 0xef, 0x54, 0x2e, 0x65, 0x79,
	0xc5, 0x0c, 0xad, 0x06, 0xd2, 0xe5, 0x78, 0xf9,
	0xf8, 0xdd, 0x68, 0x81, 0xd7, 0xdc, 0x82, 0x4d,
	0x26, 0x36, 0x0f, 0xee, 0xbf, 0x18, 0xa4, 0xfa,
	0x73, 0xe3, 0x26, 0x11, 0x22, 0x94, 0x8e, 0xfc,
	0xfd, 0x49, 0x2e, 0x74, 0xe8, 0x2e, 0x21, 0x89,
	0xed, 0x0f, 0xb4, 0x40, 0xd1, 0x87, 0xf3, 0x82,
	0x27, 0x0c, 0xb4, 0x55, 0xf2, 0x1d, 0xd1, 0x85
};

int
run_test(void)
{
	char *mech;
	uchar_t *data;
	size_t data_len;
	uchar_t *exp_digest;
	size_t exp_digest_len;
	int	rv;

	data = (uchar_t *)input_a_u;
	data_len = strlen(input_a_u);

	mech = "CKM_SHA3_224";
	exp_digest = output_a_u_224;
	exp_digest_len = 224 / BITSINBYTE;

	rv = sha3test_digest(mech, data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}

	rv = sha3test_multipart_digest(mech,
	    data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}

	mech = "CKM_SHA3_256";
	exp_digest = output_a_u_256;
	exp_digest_len = 256 / BITSINBYTE;

	rv = sha3test_digest(mech, data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}


	rv = sha3test_multipart_digest(mech,
	    data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}

	mech = "CKM_SHA3_384";
	exp_digest = output_a_u_384;
	exp_digest_len = 384 / BITSINBYTE;

	rv = sha3test_digest(mech, data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}


	rv = sha3test_multipart_digest(mech,
	    data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}

	mech = "CKM_SHA3_512";
	exp_digest = output_a_u_512;
	exp_digest_len = 512 / BITSINBYTE;

	rv = sha3test_digest(mech, data, data_len, exp_digest, exp_digest_len);

	if (rv != CRYPTO_SUCCESS) {
		return (rv);
	}

	rv = sha3test_multipart_digest(mech,
	    data, data_len, exp_digest, exp_digest_len);

	return (rv);
}


int
_init(void)
{
	int rv;
	printf("sha3test now runs\n");
	rv = run_test();
	printf("sha3test returned 0x%x, please modunload the sha3test module\n",
	    rv);

	return(mod_install(&modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return(mod_info(&modlinkage, modinfop));
}

int
_fini(void)
{
	int i;

	return(mod_remove(&modlinkage));
}
